﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Digite uma palavra e vou identificar se ela é um palíndromo:");

var text = Console.ReadLine();

var reverse = string.Join("", text.Reverse());

Console.WriteLine("Palavra informada: " + reverse);

Console.WriteLine(reverse == text ? "É um palíndromo" : "Não é um palíndromo");
